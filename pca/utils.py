import numpy as np
import h5py
import matplotlib.pyplot as plt
import corner
import matplotlib
from numpy.linalg import svd
import scipy.stats as ss
from matplotlib import gridspec
import pandas as pd
import seaborn as sns
sns.set(style="whitegrid")
import scipy.interpolate as si
import os
import glob
import bilby



def extract_dchi (data, plist):
    dchi = np.zeros([len(data),len(plist)]);
    for i in range(len(plist)):
        dchi[:,i] = data['dchi'+plist[i]];
    return dchi

def logB_from_hdf5 (filename):
    f = h5py.File(filename, 'r');
    return f['lalinference/lalinference_nest'].attrs['log_bayes_factor'] 

def extract_param (data, param):
    return data[param];


def load_hdf5 (filename):
    f = h5py.File(filename, 'r');
    posterior = f['lalinference']['lalinference_nest']['posterior_samples'][()];
    return posterior

def cov_from_pos(dchi):
    C = np.cov(np.transpose(dchi));
    return C

def reorder_dphip (dphip):
    dphip_reordered = dphip*0.0;
    for i in range(dphip.shape[1]):
        dphip_reordered[:,i] = dphip[:,dphip.shape[1]-1-i];
    return dphip_reordered;




def get_dphip_reordered (dchi):
    C = np.cov(np.transpose(dchi));
    U, S, V = svd(np.matrix(C));

    dphip = dchi*0.0;  # dphip is the set of new parameters (after svd on dchi)
    for i in range(len(dchi)):
        dphip[i,:] = np.transpose(np.matrix(V) * np.transpose(np.matrix(dchi[i,:])));
    return reorder_dphip (dphip)

def get_USV (dchi):
    C = np.cov(np.transpose(dchi));
    U, S, V = svd(np.matrix(C));
    return U,S,V

def get_dphip_from_V(V,dchi):
    dphip = dchi*0.0;  # dphip is the set of new parameters (after svd on dchi)
    for i in range(len(dchi)):
        dphip[i,:] = np.transpose(np.matrix(V) * np.transpose(np.matrix(dchi[i,:])));
    return reorder_dphip (dphip)


def make_dic_data (pos):
    if pos.shape[1] == 6:
        plist=['3','4','5l','6','6l','7']
    else:
        plist=['4','5l','6','6l','7']
    dic = {}
    for i in range(len(plist)):
        dic[plist[i]] = pos[:,i]
    return dic

def lin_comb (dchi):
    C = np.cov(np.transpose(dchi));
    U, S, V = svd(np.matrix(C));

    dphip = dchi*0.0;  # dphip is the set of new parameters (after svd on dchi)
    for i in range(len(dchi)):
        dphip[i,:] = np.transpose(np.matrix(V) * np.transpose(np.matrix(dchi[i,:])));
