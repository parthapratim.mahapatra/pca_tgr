import numpy as np
import h5py
import matplotlib.pyplot as plt
import corner
import matplotlib
from numpy.linalg import svd
import scipy.stats as ss
from matplotlib import gridspec
import pandas as pd
import seaborn as sns
sns.set(style="whitegrid")
import scipy.interpolate as si
import os
import glob
import bilby


def svd_from_margpos(
                pos,
                plist=['3','4','5l','6','6l','7'],
                category='dchi',
                caption=' ',
                nonsvd_label = r'$\delta\phi$',
                svd_label = r'$\delta\hat{\phi}^\prime$',
                injdchi = np.array([0.0, 0.0,0.0,0.0,0.0,0.0]),
                figname='figure'):
    #---------------------------------------

    dchi = pos
    C = np.cov(np.transpose(dchi));
    U, S, V = svd(np.matrix(C));

    dphip = dchi*0.0;  # dphip is the set of new parameters (after svd on dchi)
    for i in range(len(dchi)):
        dphip[i,:] = np.transpose(np.matrix(V) * np.transpose(np.matrix(dchi[i,:])));

    # computing cov for new posteriors for sanity check
    C2 = np.cov(np.transpose(dphip));
    # The diagonal of C2 should be same as S
    print ("the highest of diagonal(C2) - S  is : ", max(np.diagonal(C2) - S) )  

    # as we did SVD on COV rather than Fisher, we need to reverse the order
    # reversing the order below:
    dphip_reordered = dphip*0.0;
    for i in range(dphip.shape[1]):
            dphip_reordered[:,i] = dphip[:,dphip.shape[1]-1-i];

    # once re-ordered, we rename it
    dphip = dphip_reordered;
    C2 = np.cov(np.transpose(dphip));
    # making corner plot of dchi's (old)
    caption1 = caption + " Non-SVD non-GR deformations"
    caption2 = caption + " SVD non-GR deformations"
    labels1 = []
    labels2 = []
    for i in range(len(plist)):
            labels1 += [nonsvd_label+plist[i]];
            labels2 += [svd_label+plist[i]];

    figure = corner.corner(dchi, labels=labels1, **corner_kwargs)
    figure.gca().annotate(caption1,
                  xy=(1.0, 1.0), xycoords="figure fraction",
                  xytext=(-20, -10), textcoords="offset points",
                  ha="right", va="top")
    figure.savefig(figname+"-nonSVD-nonGR", dpi=300);

    figure = corner.corner(dphip, labels=labels2, **corner_kwargs)
    figure.gca().annotate(caption2,
                      xy=(1.0, 1.0), xycoords="figure fraction",
                      xytext=(-20, -10), textcoords="offset points",
                      ha="right", va="top")
    figure.savefig(figname+"-SVD-nonGR", dpi=300);

    return dchi, dphip, C, C2



def combined_posterior_samples(datalist, prior=[0.0,500.0], nbin=150):
    dx = np.diff(prior)[0]*1.0/nbin
    h = np.ones([nbin])
    for data in datalist:
        h1, x1 = np.histogram(data,nbin,range=(prior[0],prior[1]),density=True)
        h = h*h1
    h = h/sum(h)/dx
    
    x0 = 0.5*(x1[0:-1]+x1[1:])
    Px = si.interp1d(x0,h , kind='slinear')    
    samples = gen_random (xmin=x0[0],xmax=x0[-1],Px=Px,N=20000)    
    return samples


def stats_for_1dpos (pos1d, display = True):
    med      = round(np.percentile(pos1d,50),2)
    left_90  = round(np.percentile(pos1d,5),2)
    right_90 = round(np.percentile(pos1d,95),2)
    err_neg  = round(abs(med - left_90 ), 2)
    err_pos =  round(abs(med - right_90), 2)
    cr_90 = [left_90, right_90]
    bound = "{}^+{}_-{}".format(str(med), str(err_pos), str(err_neg))
    kde_pos1d = ss.gaussian_kde(pos1d)
    gr_at = round(kde_pos1d.integrate_box(-100,0),2)
    if display == True:
        print ("credible 90% interval: ", cr_90)
        print ("bound: ", bound)
        print ("GR at the quantile: ", gr_at)
    
    stats = {}
    stats['med'] = med
    stats['cr_90'] = cr_90
    stats['err_neg'] = err_neg
    stats['err_pos'] = err_pos
    stats['gr_at'] = gr_at
    
    return stats




def zero_recovery_quantiles(dchi, injdchi = np.array([[0.0,0.5,0.5,0.5,0.5,0.5]])):
    dphip = get_dphip_reordered (dchi)
    U, S, V = get_USV (dchi)
    inj_dphip = get_dphip_from_V(V,injdchi)
    print ("\nInjected values of PCA parameter in the decreasing order of dominance:")
    print ("---------------------------------------------------------------------")
    print (inj_dphip.flatten())
    dim = len(dchi.transpose())
    print ("\n")
    print ('Multi-dim deformation parameters before PCA:')
    print ('zero at\t\t        injection at')
    print ('quantile(sigma)\t        quantile(sigma)')
    print ('--------------------------------------------')    
    sigma_inj_dchi = []
    sigma_zero_dchi = []

    for i in range(dim):
        kde = ss.gaussian_kde(dchi[:,i])
        zero_at = kde.integrate_box(-20, 0.0)
        inj_at = kde.integrate_box(-20, injdchi[0,i])
        zero_at_sigma = abs(np.mean(dchi[:,i])/np.std(dchi[:,i]) )
        inj_at_sigma = abs( (np.mean(dchi[:,i]) - injdchi[0,i]) /np.std(dchi[:,i]) )
        sigma_zero_dchi.append(zero_at_sigma)
        sigma_inj_dchi.append(inj_at_sigma)
        print ('{:.3f}({:.2f})\t\t{:.3f}({:.2f})'.format(zero_at, zero_at_sigma, inj_at,inj_at_sigma ) )
        
    print ("\n")
    print ('Multi-dim deformation parameters after PCA:')
    print ('zero at\t\t        injection at')
    print ('quantile(sigma)\t        quantile(sigma)')
    print ('--------------------------------------------')    

    sigma_inj_dphip = []
    sigma_zero_dphip = []
    
    for i in range(dim):
        kde = ss.gaussian_kde(dphip[:,i])
        zero_at = kde.integrate_box(-20, 0.0)
        inj_at = kde.integrate_box(-20, inj_dphip[:,i])
        zero_at_sigma = abs(np.mean(dphip[:,i])/np.std(dphip[:,i]) )
        inj_at_sigma = abs((np.mean(dphip[:,i]) - inj_dphip[0,i]) /np.std(dphip[:,i]) )
        sigma_zero_dphip.append(zero_at_sigma)
        sigma_inj_dphip.append(inj_at_sigma)
        print ('{:.3f}({:.2f})\t\t{:.3f}({:.2f})'.format(zero_at, zero_at_sigma, inj_at,inj_at_sigma ) )
    return  sigma_zero_dchi, sigma_zero_dphip, sigma_inj_dchi, sigma_inj_dphip, inj_dphip



def make_dchix_frame (pos1d,paramname,label):
    dfnew = pd.DataFrame()
    dfnew['pos'] = pos1d
    dfnew['cat'] = paramname
    dfnew['Parametrization'] = label
    return dfnew

def make_violin_34567_v2 (multipos1, multipos2,
                figsavepath='violin',
                label='GW151226',
                pos1label='Non-SVD',
                pos2label='SVD',
                split_ratio = [1,1,4], ymins = [-4,-27,-35], ymaxs = [4,27,35]):
    
    violin_xlabels = [r'$\delta\phi_3$',r'$\delta\phi_4$',r'$\delta\phi_{5l}$',
                      r'$\delta\phi_6$',r'$\delta\phi_{6l}$',r'$\delta\phi_7$']
    
    # make a frame that contain elements for one violin left and right
    dflist = []
    for k in range(len(violin_xlabels)):
        df_left  = make_dchix_frame (multipos1[:,k],violin_xlabels[k],pos1label)    
        df_right = make_dchix_frame (multipos2[:,k],violin_xlabels[k],pos2label)    
        df = pd.concat([df_left, df_right], axis=0, sort=False)
        dflist.append(df)

    # splitting frames as per the figure panel split ratio
    frame_splits = []
    count = 0
    for ratio in split_ratio:
        dfnew = pd.DataFrame()
        for i in range(int(ratio)):
            dfnew = pd.concat([dfnew, dflist[count]], axis=0, sort=False)
            count += 1
        frame_splits.append(dfnew)



    #----------- The part below is still specific for the current purpose, it needs to be generalized
    fig = plt.figure(figsize=(14,5))
    gs = gridspec.GridSpec(1, len(split_ratio), width_ratios=split_ratio) 

    plt.subplot(gs[0])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
    data=frame_splits[0], palette="muted", split=True)
    ax.legend_ = None

    plt.xlabel('')
    plt.ylim(ymins[0],ymaxs[0])
    plt.ylabel(label,fontsize=20)

    plt.subplot(gs[1])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
    data=frame_splits[1], palette="muted", split=True)
    ax.legend_ = None

    plt.xlabel('')
    plt.ylim(ymins[1],ymaxs[1])

    plt.subplot(gs[2])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
                    data=frame_splits[2], palette="muted", split=True)
    plt.xlabel('')
    plt.ylim(ymins[2],ymaxs[2])
    plt.legend(fontsize=18,ncol=2)
    plt.tight_layout()
    plt.ylabel('')
    plt.savefig(figsavepath)
    
'''
eg;
make_violin_34567_v2 (multipos1=dphip_ev2r,multipos2=dphip_ev2_by_combined,
                      figsavepath=figsavepath,label='trial',
                      pos1label='SVD by itself',
                      pos2label='Transformation from combined pos',
                      split_ratio = [1,1,4])
''';








def make_violin_34567 (multipos1, multipos2,figsavepath='violin',label='GW151226',pos1label='Non-SVD',pos2label='SVD'):
    df = pd.DataFrame()

    k=0
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_3$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_3$'
    dfnew['Parametrization'] = 'SVD'
    df = pd.concat([df, dfnew], axis=0, sort=False)
    df0=df

    k=1
    df = pd.DataFrame()
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_4$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_4$'
    dfnew['Parametrization'] = pos2label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    df1 = df

    df = pd.DataFrame()
    k=2
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_{5l}$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_{5l}$'
    dfnew['Parametrization'] = pos2label
    df = pd.concat([df, dfnew], axis=0, sort=False)

    k=3
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_6$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_6$'
    dfnew['Parametrization'] = pos2label
    df = pd.concat([df, dfnew], axis=0, sort=False)

    k=4
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_{6l}$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_{6l}$'
    dfnew['Parametrization'] = pos2label
    df = pd.concat([df, dfnew], axis=0, sort=False)

    k=5
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos1[:,k]
    dfnew['cat'] = r'$\delta\phi_7$'
    dfnew['Parametrization'] = pos1label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    dfnew = pd.DataFrame()
    dfnew['pos'] = multipos2[:,k]
    dfnew['cat'] = r'$\delta\phi_7$'
    dfnew['Parametrization'] = pos2label
    df = pd.concat([df, dfnew], axis=0, sort=False)
    df2 = df

    #-----------
    fig = plt.figure(figsize=(14,5))
    gs = gridspec.GridSpec(1, 3, width_ratios=[1,1, 4]) 

    plt.subplot(gs[0])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
    data=df0, palette="muted", split=True)
    ax.legend_ = None

    plt.xlabel('')
    plt.ylim(-3,3)
    plt.ylabel(label,fontsize=20)

    plt.subplot(gs[1])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
    data=df1, palette="muted", split=True)
    ax.legend_ = None

    plt.xlabel('')
    plt.ylim(-15,15)

    plt.subplot(gs[2])
    ax = sns.violinplot(x="cat", y="pos", hue="Parametrization",
                    data=df2, palette="muted", split=True)
    plt.xlabel('')
    plt.ylim(-35,35)
    plt.legend(fontsize=18,ncol=2)
    plt.tight_layout()
    plt.ylabel('')
    plt.savefig(figsavepath)

def gen_random (xmin,xmax,Px,N=10000):
    x  = np.linspace(xmin,xmax,500)
    px = np.zeros_like(x)
    dx = x[2] - x[1]
    dPx = Px(x)
    px[0] = dPx[0] * dx
    for i in range(len(x)-1):
        px[i+1] = px[i] + dPx[i+1] *dx
    x2px  = si.interp1d(x,px , kind='slinear')
    px2x  = si.interp1d(px,x , kind='slinear')
    px_min = min(px)
    px_max = max(px)
    pxRandom = np.random.uniform(px_min,px_max,N)
    xrandom = px2x(pxRandom)
    return xrandom

